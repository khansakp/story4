from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse

from .models import Schedule
from . import forms


def homepage(request):
    return render(request, 'homepage.html')

def more(request):
    return render(request, 'more.html')

def portfolio(request):
    return render(request, 'portfolio.html')

def schedule(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def addschedule(request):
    if request.method == 'POST': 
        form = forms.scheduleForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/schedule')

    else:
        form = forms.scheduleForm()
    return render(request, 'addschedule.html', {'form': form})

def resetAll(request):
    Schedule.objects.all().delete()
    schedules =[]
    return render(request, 'schedule.html', {'schedules': schedules})


# Create your views here.
