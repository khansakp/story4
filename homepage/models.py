from django.db import models
from datetime import datetime

# Create your models here.

class Schedule(models.Model):

    activity = models.CharField(max_length=15)
    description = models.CharField(max_length=40)
    date = models.DateTimeField(default=datetime.now)
    time = models.TimeField()
    place = models.CharField(max_length=15)
    category = models.CharField(max_length=20)