from django.conf.urls import url
from . import views
from django.urls import path


app_name = 'homepage'

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('more/', views.more, name='more'),
    path('portfolio/', views.portfolio, name='portfolio'),
    path('schedule/', views.schedule, name = 'schedule'),
    path('addschedule/', views.addschedule, name = 'addschedule'),
    path('resetAll/',views.resetAll,name = "resetAll"),
]